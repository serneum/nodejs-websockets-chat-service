# Using Node.js and Websockets to Build a Chat Service

The source code for the Nettuts+ article, "Using Node.js and Websockets to Build a Chat Service" by Guillaume Besson.

To run the chat service, first execute
```
npm install
```

After the command completes, execute
```
node server.js
```

Point your web browser to localhost:3000
